# Excel transform project

## Installation

1. Install pip `sudo apt install python-pip`
2. Install virtualenv `sudo pip install virtualenv`
3. Go to project for python  `cd python`
4. Create Python enviroments  `virtualenv venv --python=python3.9`
5. Activate virtual env `source venv/bin/activate`
6. Install dependencies `pip install -r requirements.txt`


#### To run:

`main.py`