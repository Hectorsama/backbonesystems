<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{

    protected $visible = ['key', 'name','zone_type','settlementType'];
    protected $primaryKey = 'key';

    public $timestamps = false;

    public function settlementType()
    {
        return $this->belongsTo('App\Models\SettlementType', 'settlement_type');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }
}
