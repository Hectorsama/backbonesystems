<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettlementType extends Model
{
    protected $primaryKey = 'key';
    public $timestamps = false;

    protected $visible = ['name'];
}
