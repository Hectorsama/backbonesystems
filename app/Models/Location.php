<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $primaryKey = 'zip_code';
    public $timestamps = false;

    protected $casts = [
        'zip_code' => 'string',
    ];
    
    public function settlements()
    {
        return $this->hasMany('App\Models\Settlement');
    }


    public function federalEntity()
    {
        return $this->belongsTo('App\Models\FederalEntity', 'federal_entity');
    }
    public function municipality()
    {
        return $this->belongsTo('App\Models\Municipality', 'municipality');
    }
}
