<?php

namespace App\Http\Helper;

class ZipCode
{

    /**
     *  Return a sanitize string
     *
     * @param  string 
     * @return string
     */
    public static function rename($zipcode)
    {
        return strlen($zipcode) == 4 ? substr_replace($zipcode, "0", 0, 0) : $zipcode;

    }
}