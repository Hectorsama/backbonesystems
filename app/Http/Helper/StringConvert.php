<?php

namespace App\Http\Helper;

class StringConvert
{

    /**
     *  Return a sanitize string
     *
     * @param  string 
     * @return string
     */
    public static function rename($str)
    {

        $transliterator = \Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: NFC;', \Transliterator::FORWARD);
        return strtoupper($transliterator->transliterate($str));
    }
}