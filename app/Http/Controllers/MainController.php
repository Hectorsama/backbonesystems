<?php

namespace App\Http\Controllers;

use App\Http\Helper\ZipCode;
use App\Models\Location;
use Illuminate\Http\Request;

class MainController extends Controller
{


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $zipCode = ZipCode::rename($request->zip_code);
        return Location::with('federalEntity', 'settlements', 'settlements.settlementType', 'municipality')->where('zip_code', $zipCode)->first();
    }

}