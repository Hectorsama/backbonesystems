<?php

namespace App\Imports;

use App\Http\Helper\StringConvert;
use App\Http\Helper\ZipCode;
use App\Models\Settlement;
use App\Models\SettlementType;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ImportSettlements implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        $settlementType = SettlementType::where('name', $row['d_tipo_asenta'])->value('key');

        return new Settlement([
            'key'=>$row['id_asenta_cpcons'] ,
            'name' => StringConvert::rename($row['d_asenta']),
            'zone_type' => StringConvert::rename($row['d_zona']),
            'settlement_type' => $settlementType,
            'location_zip_code' => ZipCode::rename($row['d_codigo']),
        ]);
    }


    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

}