<?php

namespace App\Imports;

use App\Http\Helper\StringConvert;
use App\Models\FederalEntity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ImportFederalEntity implements ToModel, WithHeadingRow, WithBatchInserts, WithUpserts, WithChunkReading,ShouldQueue,WithProgressBar
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    use Importable;
    public function model(array $row)
    {
        return new FederalEntity([
            'key'=>$row['c_estado'],
            'name' => StringConvert::rename($row['d_estado']),
        ]);

    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 10000;
    }

    public function uniqueBy()
    {
        return 'name';
    }



}