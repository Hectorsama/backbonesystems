<?php

namespace App\Imports;

use App\Http\Helper\StringConvert;
use App\Http\Helper\ZipCode;
use App\Models\FederalEntity;
use App\Models\Location;
use App\Models\Municipality;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class ImportLocation implements ToModel, WithHeadingRow, WithBatchInserts, WithUpserts, WithChunkReading
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

        $federalEntity = FederalEntity::where('name', StringConvert::rename($row['d_estado']))->value('key');
        print_r($row);
        echo $row['d_mnpio'];
        $municipality = Municipality::where('name', StringConvert::rename($row['d_mnpio']))->value('id');
        return new Location([
            'zip_code' => ZipCode::rename($row['d_codigo']),
            'locality' => $row['d_ciudad'] != null ? StringConvert::rename($row['d_ciudad']) : StringConvert::rename($row['d_estado']),
            'federal_entity' => $federalEntity,
            'municipality' => $municipality
        ]);

    }


    public function batchSize(): int
    {
        return 100;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function uniqueBy()
    {
        return 'zip_code';
    }

}