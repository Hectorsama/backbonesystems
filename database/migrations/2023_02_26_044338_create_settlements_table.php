<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlements', function (Blueprint $table) {
            $table->id();
            $table->integer('key');
            $table->string('name');
            $table->string('zone_type');
            $table->string('location_zip_code');
            $table->unsignedBigInteger('settlement_type');
        });

        Schema::table('settlements', function ($table) {
            $table->foreign('settlement_type')->references('key')->on('settlement_types')->onDelete('cascade');
            $table->foreign('location_zip_code')->references('zip_code')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlements');
    }
}