<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->string('zip_code')->primary();
            $table->string('locality');
            $table->unsignedBigInteger('federal_entity');
            $table->unsignedBigInteger('municipality');
            
        });

        Schema::table('locations', function($table) {
            $table->foreign('federal_entity')->references('key')->on('federal_entities')->onDelete('cascade');
            $table->foreign('municipality')->references('id')->on('municipalities')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}