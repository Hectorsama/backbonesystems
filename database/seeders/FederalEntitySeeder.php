<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\File;
use App\Imports\ImportFederalEntity;
use Illuminate\Database\Seeder;

class FederalEntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $path = public_path('codes');
        $files = File::allFiles($path);

        foreach ($files as $file) {
            \Excel::import(
                new ImportFederalEntity,
                public_path('codes/' . $file->getRelativePathname())
            );
        }


    }
}