<?php

namespace Database\Seeders;

use App\Imports\ImportLocation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('codes');
        $files = File::allFiles($path);

        foreach ($files as $file) {
            \Excel::import(
                new ImportLocation,
                public_path('codes/' . $file->getRelativePathname())
            );
        }

    }
}
