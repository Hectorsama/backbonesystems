<?php

namespace Database\Seeders;

use App\Imports\ImportSettlementType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class SettlementTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('codes');
        $files = File::allFiles($path);

        foreach ($files as $file) {
            \Excel::import(
                new ImportSettlementType,
                public_path('codes/' . $file->getRelativePathname())
            );
        }
    }
}